#include <string.h>
#include "commun.h"

void err_normal(char *msg, const char *context){
    if (context != NULL){
        fprintf(stderr, "%s: %s\n", context, msg);
    }
    else{
        fprintf(stderr, "Erreur: %s\n", msg);
    }
}

void err_fatal(int code, const char *msg, const char *context){
    fprintf(stderr, "Erreur Fatale: ");
    if (context != NULL)
        fprintf(stderr, "%s: ", context);
    fprintf(stderr, "%s\n", msg);

    exit(code);
}

char maybe_scanf_int(int *val, const char *err_context) {
    char buf[10 + 1 + 5];
    
    while (1) {
        if (fgets(buf, sizeof(buf) / sizeof(char), stdin) == NULL) {
            // quelque chose s'est mal passé
            if (feof(stdin)) {
                // EOF c'est normal, on ne lis rien
                return 0;
            } else {
                // autre, ce n'est pas normal, erreur
                err_fatal(-1, "erreur lors de la lecture de l'entree standard", NULL);
            }
        }
        
        int r;
        r = sscanf(buf, "%d%*c", val);
        if (r == 1) {
            // Ok
            return 1;
        } else if (r == EOF) {
            // On a atteint la fin du buffer...
            if (strchr(buf, '\n') != NULL) {
                // Il y a un retour a la ligne, ce qui signifie que l'utilisateur a volontairement ignoré le champ.
                return 0;
            }
            
            // Il n'y a pas eu de retour a la ligne, ce qui veut que la ligne y a réellement rien a lire OU que la valeur n'est pas dans le buffer.
        } else {
            // Err
            err_normal("nombre entier invalide", err_context);
        }
    }
}

char *suppr_retour_a_la_ligne(char *str) {
    char *lf = strchr(str, '\n');
    if (lf != NULL)
        *lf = '\0';
    return str;
}

char ask(char *question) {
    char a = 'n';
    printf("%s [Y/n]\n", question);
    scanf("%c", &a);
    
    return a == 'Y' || a == 'y';
}

/* void debug_string(char *str) {
    while(*str) {
        printf("%02x ", (unsigned int) *str);
        str ++;
    }
    printf("%02x\n", 0);
} */
