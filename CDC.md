Cahier des charges
==================

Nous avons choisi de faire un complexe sportif.
Les activités seront proposées a des prix (nombre de points varie en fonction des activités et dois pouvoir être reconfiguré au cours du temps).
Les activités seront également proposées en mode découverte: Le mode découverte est disponible 1 fois / adhérent (ou carte?) et permet à ce dernier de faire l'activité à moitié prix.
Les adhérents ne peuvent pas fréquenter le centre plus d'une fois par jour.
La gestion des jours se fera manuellement (pour éviter les soucis liés à l'arrêt & démarrage du programme).
De plus, un adhérent ne peut faire une activité qu'une seule fois par jour. Mais peut en faire tout les jours s'il le souhaite.
Pour conserver la motivation un système de points bonus est présent pour chaque activité. Ce système est cumulatif: au bout de x participations à une activité (cela dépend de l'activité, cela peut être reconfiguré) l'adhérent reçoit un point bonus sur sa carte (et le compteur pour cette activité est remise à zéro). Cela permet également d'amener les adhérents a diversifier leurs activités.

Fonctionnalités
--------------

Voici la liste des fonctionnalités que nous voulons implémenter, ainsi que leur détails :

1. Afficher tous les adhérents

    Quand l'utilisateur tape 1 dans le menu cela affiche la liste des adhérents sous la forme: id \[tab\] nom \[tab\] prénom.

2. Afficher les infos d'un adhérent

    Quand l'utlisateur tape 2, sera ensuite demandé l'identifiant de l'adhérent qui affichera le détail des informations associés:
    id nom prénom datenaissance email numero adresse idcarte

3. Créer un adhérent

    Quand l'utilisateur tape 3, sera ensuite demandé les champs détaillés suivants:
    id nom prénom datenaissance email numero adresse

    Une fois opération faite, l'id de la carte associée sera donnée

4. Mettre a jour un adhérent

    Quand l'utilisateur tape 4, sera ensuite demandé les champs détaillés suivants:
    id nom prénom datenaissance email numero adresse

    Ne rien mettre pour un champ équivant a ne laisser dans son état actuel. (ne pas changer)

5. Supprimer un adhérent

    Sera demandé l'id de l'adhérent puis en guise de confirmation de retaper le **nom** de l'adhérent pour confirmer la suppression.

6. Alimenter une carte

    sera demandé l'id de la carte puis le nombre de points a recharger
    affiche le montant correspondant
    puis demande confirmation du rechargement (et du virement)

7. Créer une carte

    Lors de la création d'une carte, seul l'id de l'adhérent associé est demandé.
    L'id unique de la carte est déterminé automatiquement par le programme.
    Une fois la carte crée, l'id sera donné à l'utilisateur pour des manipulations futures.
    *Une carte est créé mais n'est pas activée par défaut.*

8. Désactiver/Activer une carte (déclaration de perte d’une carte, sanction suite à un mauvais comportement, carte retrouvée, ...)

    Permet de rendre une carte utilisable ou non.
    l'id de la carte sera demandé et indiquera si la carte était déjà activée ou non.

9. Détruire une carte

    Demande l'id de la carte a détruire (supprimer de la base de donnée).
    Par sécurité, cette opération nécessite que la carte soit désactivée.

10. Afficher le nombre d’entrées par activité dans la journée

    Affiche sous forme de tableau le nombre d'entrées par activité
    et le nombre total d'entrée.

11. Lister les activité

12. Existence de l'activité

13. Ajouter une activité

14. Changer le nombre de points d'une activité

15. Supprimer une activité

16. Séance découverte (1 fois / activité / personne) (50% réduction)

    La séance découverte est disponible 1 fois par personne et par activité.

Activités proposées
-------------------

2pts = 1€

* Musculation         10pts
* Natation            9pts
* Volley Ball         10pts
* Escalade            15pts
* Parcourt            7pts
* Badminton           8pts
* Tennis              8pts
* Hand ball           8pts
* Ping Pong           7pts
* Pelote Basque       4pts
* Pétanque            5pts
* Chasetag            10pts
* Foot                10pts
* Rugby               10pts
* Cyclisme sur piste  15pts
* Saut en parachute   20pts
* Aquaponey           69pts
* Plongée en fausse   20pts

Répartition du travail dans le groupe
-------------------------------------

Parties communes :
    - Conception du cahier des charges
    - Conception du fichier conception
    - Conception de la doc de Doxyfile
    - Conception du README
    - Conception du main (au niveau code)
    - Conception de la table des erreurs

Partie Mathis Delage :
    - Conception de la partie Activité (au niveau code)

Partie Mathieu Grousseau :
    - Conception de la partie Cartes (au niveau code)

Partie Mathis Moulin :
    - Conception de la partie Adhérent (au niveau code)
