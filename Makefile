all: bin/main.o bin/activite.o bin/adherant.o bin/carte.o bin/commun.o
	gcc -Wall bin/main.o bin/activite.o bin/adherant.o bin/carte.o bin/commun.o -o bin/sae

bin/main.o: src/main.c bin/commun.o
	gcc -Wall -c src/main.c -o bin/main.o

bin/activite.o: src/activite.c src/activite.h bin/commun.o
	gcc -Wall -c src/activite.c -o bin/activite.o

bin/adherant.o: src/adherant.c src/adherant.h bin/commun.o
	gcc -Wall -c src/adherant.c -o bin/adherant.o

bin/carte.o: src/carte.c src/carte.h bin/commun.o
	gcc -Wall -c src/carte.c -o bin/carte.o

bin/commun.o: src/commun.c src/commun.h
	gcc -Wall -c src/commun.c -o bin/commun.o

doc: src/main.c src/activite.h src/adherant.h src/carte.h src/commun.h
	doxygen

clean:
	rm bin/main.o bin/activite.o bin/adherant.o bin/carte.o bin/commun.o bin/sae & rm -r doc/
