#include "activite.h"
#include "commun.h"

const char * PATH_ACTIVITE = "data/activite";
const int MAX_SIZE_PTS = 3;

//Private Functions
void demande_nom(char *nom_act){
    int i = 0, change = 0;

    printf("\nEntrez le nom de l'activite: ");
    fgets(nom_act, MAX_SIZE_ACT, stdin);
    
    while (i < strlen(nom_act)){
        if (nom_act[i] == '\n'){
            change = 1;
        }
        if (change == 1){
            nom_act[i] = nom_act[i+1];
        }
        ++i;
    }
}

void demande_point(int *points){
    printf("\nEntrez le nombre de point: ");
    scanf("%d", points);
}

int charger_act(char list_act[][MAX_SIZE_ACT], int list_pts[], int TMAX) {
        FILE *activite_file = fopen(PATH_ACTIVITE, "r");
        int i = 0;

        if (activite_file == NULL){
            err_fatal(1, "Impossible de lire 'activite'", "Charger activite");
        }

        while (!feof(activite_file)){
            if (i>=TMAX){
                err_fatal(2, "Tableau trop petit", "Charger activite");
            }
            fscanf(activite_file, "%s %d", list_act[i], &list_pts[i]);
            ++i;
        }
        fclose(activite_file);

        return i;
}

void save_act(char list_act[][MAX_SIZE_ACT], int list_pts[], int Tlog){
    FILE *activite_file = fopen(PATH_ACTIVITE, "w");
    int i = 0;

    while (i < Tlog-1){
        fprintf(activite_file, "%s %3d\n", list_act[i], list_pts[i]);
        ++i;
    }
    fclose(activite_file);
}


//Public Functions
void commande_exist_act(void){
    char nom_act_cherche[MAX_SIZE_ACT];
    int retour, position = 0;

    printf("Fonction de recherche\n");

    printf("Recherche de: %20s\n", nom_act_cherche);

    retour = activite_exist(nom_act_cherche, &position);

    if (retour == 0) {
        printf("L'activite %s n'a pas ete trouve\n", nom_act_cherche);
    } else if (retour == 1) {
        printf("L'activite %s a ete trouvee a la ligne %d\n", nom_act_cherche, position+1);
    }

}

int activite_exist(char *nom_act, int *position){
    FILE *activite_file = fopen(PATH_ACTIVITE, "r");
    int i = 0;

    if (activite_file == NULL){
        err_normal("Impossible d'ouvrir 'activite'", "Activite exist");
        return -1;
    }
    else{
        char nom_act_cur[MAX_SIZE_ACT];
        int pts;
    
        while (1) {
            fscanf(activite_file, "%s %d", nom_act_cur, &pts);
            
            if (feof(activite_file))
                break;
            
            if (strcmp(nom_act, nom_act_cur) == 0){
                fclose(activite_file);
                *position = i;
                return 1;
            }
            ++i;
        }
        fclose(activite_file);
        return 0;
    }
}


void commande_list_act(void){
    int list_pts[MAX_ACT];
    char list_act[MAX_ACT][MAX_SIZE_ACT];
    int Tlog = 0;

    Tlog = charger_act(list_act, list_pts, MAX_ACT);

    for (int j = 0; j < Tlog-1; ++j){
        printf("%20s: %3d\n", list_act[j], list_pts[j]);
    }
}


void commande_ajout_activite(void){
    char nom[MAX_SIZE_ACT];
    int points;
    
    printf("Fonction ajout d'une activite\n");

    demande_nom(nom);
    demande_point(&points);

    ajout_activite(nom, points);
}

void ajout_activite(char *nom_act, int nb_point){
    int position;
    int act_ex = activite_exist(nom_act, &position);

    if (act_ex == 1){
        err_normal("Activite deja enregistre", "Ajout activite");
    }
    else{
        if (act_ex == -1){
            printf("Creation de activite\n");
        }
        FILE *activite_file = fopen(PATH_ACTIVITE, "a");
        fprintf(activite_file, "%s %3d\n", nom_act, nb_point);
        fclose(activite_file);
    }
}


void commande_modif_point(void){
    char nom[MAX_SIZE_ACT];
    int points;

    printf("Fonction modification d'une activite\n");

    demande_nom(nom);
    demande_point(&points);

    modif_point(nom, points);
}

void modif_point(char *nom_act, int nv_point){
    int position;
    int act_ex = activite_exist(nom_act, &position);

    if (act_ex == 0){
        err_normal("L'activite n'existe pas ajoute la, avant de modifie son nombre de point", "Modification point");
    }
    else{
        int list_pts[MAX_ACT];
        char list_act[MAX_ACT][MAX_SIZE_ACT];
        int Tlog, i=0;

        Tlog = charger_act(list_act, list_pts, MAX_ACT);

        while (i != Tlog){
            if (strcmp(list_act[i], nom_act) == 0){
                list_pts[i] = nv_point;
            }
            ++i;
        }
        save_act(list_act, list_pts, Tlog);
    }
}


void commande_suppr_act(void){
    char nom[MAX_SIZE_ACT];
    char conf_nom[MAX_SIZE_ACT];

    printf("Fonction suppresion d'une activite\n");

    demande_nom(nom);
    printf("Confirmation de suppression:\n");
    demande_nom(conf_nom);
    if (strcmp(conf_nom, nom) == 0){
        suppr_act(nom);
    }
    else{
        printf("Confirmation de suppression echoue\n");
    }

}

void suppr_act(char *nom_act){
    int position;
    int exist = activite_exist(nom_act, &position);

    if (exist == -1){
        err_fatal(1, "Le fichier activite n'est pas lisible", "Supprimer activite");
    }

    else if (exist == 0){
        err_normal("L'activite n'existe pas ajoute la, avant de la supprimer", "Supprimer activite");
    }
    else{
        char list_activite[MAX_ACT][MAX_SIZE_ACT];
        int list_pts[MAX_ACT];
        int Tlog;

        Tlog = charger_act(list_activite, list_pts, MAX_ACT);
        for (int i = position; i < Tlog - 1; i++){
            strcpy(list_activite[i], list_activite[i+1]);
            list_pts[i] = list_pts[i+1];
        }
        --Tlog;
        save_act(list_activite, list_pts, Tlog);
        printf("Activite %s supprime\n", nom_act);
    }
}

void charge_stats_generic(const char *fichier, int t_capacity, int *t_length, char t_act_nom[][MAX_SIZE_ACT], int t_act_size[], int t_act_adh[][MAX_STATS_ENTRY_SIZE], int t_act_cnt[][MAX_STATS_ENTRY_SIZE]) {
    FILE *f = fopen(fichier, "r");
    if (f == NULL)
        err_fatal(ERR_CODE_FAIL_FICHIER, "Impossible d'ouvrir le ficier de donnee", "statistiques");
    
    *t_length = 0;
    
    while (1) {
        if (fscanf(f, "%s %d", t_act_nom[*t_length], &t_act_size[*t_length]) != 2) {
            if (feof(f))
                break;
            
            fclose(f);
            err_fatal(4, "Erreur de lecture des donnees", "statistiques");
        }
        
        if (*t_length >= t_capacity) {
            fclose(f);
            err_fatal(ERR_CODE_TABLE_SATUREE, "Tableaux satures", "charger statistiques");
        }
        
        int i;
        for (i = 0; i < t_act_size[*t_length]; i++) {
            if (fscanf(f, " %d %d", &t_act_adh[*t_length][i], &t_act_cnt[*t_length][i]) != 2 || feof(f)) {
                fclose(f);
                err_fatal(4, "Erreur de lecture des donnees", "statistiques");
            }
        }
        
        fscanf(f, "; ");
        
        *t_length += 1;
        
        /* if (feof(f)) {
            fclose(f);
            err_fatal(-1, "Erreur de lecture des donnees", "statistiques");
        } */
    }
    
    fclose(f);
}

void sauvegarde_stats_generic(const char *fichier, int t_length, char t_act_nom[][MAX_SIZE_ACT], int t_act_size[], int t_act_adh[][MAX_STATS_ENTRY_SIZE], int t_act_cnt[][MAX_STATS_ENTRY_SIZE]) {
    FILE *f = fopen(fichier, "w");
    if (f == NULL)
        err_fatal(ERR_CODE_FAIL_FICHIER, "Impossible d'ouvrir le ficier de donnee", "statistiques");
    
    int i;
    for (i = 0; i < t_length; i++) {
        if (fprintf(f, "%s %d", t_act_nom[i], t_act_size[i]) < 0) {
            fclose(f);
            err_fatal(-1, "erreur lors de l'ecriture des donnes", "statistiques");
        }
        
        int j;
        for (j = 0; j < t_act_size[i]; j++) {
            if (fprintf(f, " %d %d", t_act_adh[i][j], t_act_cnt[i][j]) < 0) {
                fclose(f);
                err_fatal(-1, "erreur lors de l'ecriture des donnes", "statistiques");
            }
        }
        
        if (fprintf(f, ";\n") < 0) {
            fclose(f);
            err_fatal(-1, "erreur lors de l'ecriture des donnes", "statistiques");
        }
    }
    
    fclose(f);
}

void reset_stats(const char *fichier) {
    FILE *f = fopen(fichier, "w");
    if (f == NULL)
        err_fatal(ERR_CODE_FAIL_FICHIER, "Impossible d'ouvrir le ficier de donnee", "statistiques");
    
    ;
    
    fclose(f);
}


void increment_stat(const char *fichier, int adherent, const char* activite) {
    char t_act_nom[MAX_ACT][MAX_SIZE_ACT];
    int t_act_size[MAX_ACT], t_act_adh[MAX_ACT][MAX_STATS_ENTRY_SIZE], t_act_cnt[MAX_ACT][MAX_STATS_ENTRY_SIZE];
    int len;
    charge_stats_generic(fichier, MAX_ACT, &len, t_act_nom, t_act_size, t_act_adh, t_act_cnt);
    
    int i;
    for (i = 0; i < len; i++) {
        if (strcmp(t_act_nom[i], activite) == 0) {
            int j;
            for (j = 0; j < t_act_size[i]; j++) {
                if (t_act_adh[i][j] == adherent) {
                    t_act_cnt[i][j] += 1;
                    sauvegarde_stats_generic(fichier, len, t_act_nom, t_act_size, t_act_adh, t_act_cnt);
                    return;
                }
            }
            
            if (t_act_size[i] >= MAX_STATS_ENTRY_SIZE)
                err_fatal(ERR_CODE_TABLE_SATUREE, "Tableau satures", "decouvre");
            
            t_act_adh[i][t_act_size[i]] = adherent;
            t_act_cnt[i][t_act_size[i]] = 1;
            t_act_size[i] += 1;
            
            sauvegarde_stats_generic(fichier, len, t_act_nom, t_act_size, t_act_adh, t_act_cnt);
            return;
        }
    }
    
    if (len >= MAX_ACT)
        err_fatal(ERR_CODE_TABLE_SATUREE, "Tableau satures", "decouvre");

    strcpy(t_act_nom[len], activite);
    t_act_size[len] = 1;
    t_act_adh[len][0] = adherent;
    t_act_cnt[len][0] = 1;
    len++;
    
    sauvegarde_stats_generic(fichier, len, t_act_nom, t_act_size, t_act_adh, t_act_cnt);
}

int get_stat(const char *fichier, int adherent, const char* activite) {
    char t_act_nom[MAX_ACT][MAX_SIZE_ACT];
    int t_act_size[MAX_ACT], t_act_adh[MAX_ACT][MAX_STATS_ENTRY_SIZE], t_act_cnt[MAX_ACT][MAX_STATS_ENTRY_SIZE];
    int len;
    charge_stats_generic(fichier, MAX_ACT, &len, t_act_nom, t_act_size, t_act_adh, t_act_cnt);
    
    int i;
    for (i = 0; i < len; i++) {
        if (strcmp(t_act_nom[i], activite) == 0) {
            int j;
            for (j = 0; j < t_act_size[i]; j++) {
                if (t_act_adh[i][j] == adherent) {
                    return t_act_cnt[i][j];
                }
            }
            
            break;
        }
    }
    
    return 0;
}
