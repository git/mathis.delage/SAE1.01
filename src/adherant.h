/**
 * \file adherant.h
 * \brief Module concernant les adhérents.
 * \author Moulin Mathis
 * 
 * Ce fichier regroupe les commandes concernant les adhérents ainsi que des 
 * fonctions pour manipuler les données des adhérents depuis d'autres modules.
 */

#include "commun.h"
#include <string.h>

#define TAILLE_MAX_FICHIER 200
#define TAILLE_MAX_NOM 20
#define TAILLE_MAX_PRENOM 30
#define TAILLE_MAX_ADRESSE_EMAIL 100
#define TAILLE_MAX_ADRESSE 100

#define PAS_DE_CARTE -1

/**
 * \brief Crée et ajoute un adhérent.
 *
 * Cette commande créé un adhérent et l'ajout simplement au fichier 'adherant.txt'.
 */
void commande_creer_adherent(void);   

/**
 * \brief Supprime un adhérent.
 *
 * Cette commande demande l'identifiant de l'adhérent et le supprime.
 */ 
void commande_suppimer_adherent(void);

/**
 * \brief Affiche un adhérent.
 *
 * Cette commande demande l'identifiant de l'adhérent et l'affiche avec ses infos de base.
 */ 
void commande_affiche_adherent(void);

/**
 * \brief Affiche tous les adhérents.
 *
 * Cette commande affiche tous les adhérents avec ses infos de base.
 */ 
void commande_affiche_tous_adherents(void);

/**
 * \brief Affiche un adhérent.
 *
 * Cette commande demande l'identifiant de l'adhérent',
 * et affiche toutes ses informations personnelles.
 */ 
void commande_infos_adherent(void);

/**
 * \brief Met à jour un adhérent.
 *
 * Cette commande demande l'identifiant de l'adhérent',
 * Demande une sélection d'un numérique (entre 0 et 7) pour choisir quoi modifier.
 */ 
void commande_maj_adherent(void);

/**
 * \brief Cherche l'indice d'un adhérent dans le tableau à partir de son identifiant unique
 * \param [in] ids Le tableau des identifiants des adhérents
 * \param longueur La taille du tableau
 * \param id L'identifiant pour lequel l'indice est cherché
 * \param [out] trouve Si la ligne a été trouvée
 * \return L'indice de l'adhérent, ou sa position d'insertion s'il n'est pas dans le tableau
 */
int indice_adherent(const int ids[], int longueur, int id, char *trouve);

/**
 * \brief Charge les données des adhérents.
 *
 * \param [in out] increment Pointeur sur un entier qui permet d'incrémenter ou de savoir qu'elle va être le prochain id / idcarte 
 * \param capacite Capacite maximale des tableaux
 * \param [in out] longueur Pointeur sur un entier qui permet de savoir combien de ligne d'adhérent du fichier 'adherant.txt' 
 * \param [out] id Tableau des identifiants uniques des adhérents
 * \param [out] nom Tableau des noms des adhérents
 * \param [out] prenom Tableau des prenoms des adhérents
 * \param [out] idcarte Tableau des idcartes uniques des adhérents
 * \param [out] adresse_email Tableau des adresses emails des adhérents
 * \param [out] adresse Tableau des adresses des adhérents
 * \param [out] code_postal Tableau des codes postaux des adhérents
 * \param [out] num_telephone Tableau des numéros de téléphones des adhérents
 *
 */
void charger_fichier_adherant(int * increment, int capacite, int * longueur, int id[], char nom[][TAILLE_MAX_NOM], char prenom[][TAILLE_MAX_PRENOM], int idcarte[], char adresse_email[][TAILLE_MAX_ADRESSE_EMAIL], char adresse[][TAILLE_MAX_ADRESSE], int code_postal[], int num_telephone[]);


/**
 * \brief Sauvegarde les données des adhérents.
 * \param increment Pointeur sur un entier qui permet d'incrémenter ou de savoir qu'elle va être le prochain id / idcarte 
 * \param longueur Pointeur sur un entier qui permet de savoir combien de ligne d'adhérent du fichier 'adherant.txt' 
 * \param [in] id Tableau des identifiants uniques des adhérents
 * \param [in] nom Tableau des noms des adhérents
 * \param [in] prenom Tableau des prenom des adhérents
 * \param [in] idcarte Tableau des idcartes uniques des adhérents
 * \param [in] adresse_email Tableau des adresses emails des adhérents
 * \param [in] adresse Tableau des adresses des adhérents
 * \param [in] code_postal Tableau des codes postals des adhérents
 * \param [in] num_telephone Tableau des numéros de téléphones des adhérents
 */
void sauvegarde_fichier_adherant(int increment, int longueur, int id[], char nom[][TAILLE_MAX_NOM], char prenom[][TAILLE_MAX_PRENOM], int idcarte[], char adresse_email[][TAILLE_MAX_ADRESSE_EMAIL], char adresse[][TAILLE_MAX_ADRESSE], int code_postal[], int num_telephone[]);
