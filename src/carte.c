#include "commun.h"
#include "carte.h"
#include "adherant.h"

// TODO: mettre les messages d'erreurs communs dans des constantes ou statiques vu que c des strings?
// TODO: débitement ?
// TODO: points bonus (cumulation) ?

const char *FICHIER_CARTES = "data/cartes";

/**
 * \brief Calcule le prochain identifiant unique pour les cartes
 * \param [in] t_carte_ids La colonne des identifiants uniques
 * \param len la taille du tableau
 * \return Le prochain identifiant unique a allouer
 */
int trouver_prochain_uid_libre(const int t_carte_ids[], int len);

/**
 * \brief Insère une ligne à l'indice donné avec les valeurs données.
 * \param [in,out] t_carte_ids Tableau des identifiants uniques des cartes
 * \param [in,out] t_carte_act Tableau des états d'activation des cartes
 * \param [in,out] t_carte_adh Tableau des identifiants des adhérents par carte
 * \param [in,out] t_carte_pts Tableau des points par carte
 * \param [in,out] len Taille des tableaux
 * \param index La position d'insertion
 * \param new_id L'id unique a insérer
 * \param new_act L'état d'activation de la carte
 * \param new_adh L'id unique du détenteur de la carte
 * \param new_pts Le nomnre de points sur la carte
 * \warning Cette fonction ne se préoccupe pas de la capacité des tableaux. Il 
 *          est de la responsabilité des appeleurs de le vérifier.
 */
void t_carte_insert(int t_carte_ids[], char t_carte_act[], int t_carte_adh[], int t_carte_pts[], /* int capacite, */ int *len, int index, int new_id, char new_act, int new_adh, int new_pts);
/**
 * \brief Retire la ligne à l'indice donné.
 * \param [in,out] t_carte_ids Tableau des identifiants uniques des cartes
 * \param [in,out] t_carte_act Tableau des états d'activation des cartes
 * \param [in,out] t_carte_adh Tableau des identifiants des adhérents par carte
 * \param [in,out] t_carte_pts Tableau des points par carte
 * \param [in,out] len Taille des tableaux
 * \param index La position d'insertion
 */
void t_carte_remove(int t_carte_ids[], char t_carte_act[], int t_carte_adh[], int t_carte_pts[], int *len, int index);

void commande_ajouter_carte(void) {   
    char t_carte_act[TAILLE_TABLEAUX]; 
    int t_carte_ids[TAILLE_TABLEAUX], t_carte_adh[TAILLE_TABLEAUX], t_carte_pts[TAILLE_TABLEAUX], len;
    int r = charger_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, TAILLE_TABLEAUX, &len);
    if (r == 1) {
        err_fatal(-1, "capacite maximale memoire atteinte impossible de rajouter une entree", COMMANDE_AJOUTER_CARTE);
    } else if (r != 0)
        err_fatal(-1, "erreur lors du chargement des donnes de cartes", COMMANDE_AJOUTER_CARTE);
    
    int new_uid = trouver_prochain_uid_libre(t_carte_ids, len);
    
    printf("Identifiant de l'adherent a associer: ");
    int id_adher;
    if (!maybe_scanf_int(&id_adher, COMMANDE_AJOUTER_CARTE)) {
        printf("Operation annulee.\n");
        return;
    }
    
    {    
        int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
        char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM], adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];
        
        int increment, adh_len;
        charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &adh_len, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
        
        char adh_existe;
        int adh_index = indice_adherent(id, adh_len, id_adher, &adh_existe);
        
        if (!adh_existe) {
            err_normal("Il n'y a pas d'adherent avec cet identifiant.", COMMANDE_ACTIVER_CARTE);
            return;
        }
        
        idcarte[adh_index] = new_uid;
        
        // insérer à la fin 
        t_carte_insert(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, &len, len, new_uid, 0/*FALSE*/, id_adher, 0);
        
        sauvegarde_fichier_adherant(increment, adh_len, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
        sauvegarder_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, len);
        
        printf("Nouvelle carte:\n\tid: %d\n\ttitulaire: %s %s (%d)\n", new_uid, nom[adh_index], prenom[adh_index], id_adher);
    }
}

void commande_supprimer_carte(void) {
    char t_carte_act[TAILLE_TABLEAUX];
    int t_carte_ids[TAILLE_TABLEAUX], t_carte_adh[TAILLE_TABLEAUX], t_carte_pts[TAILLE_TABLEAUX], len;
    int r = charger_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, TAILLE_TABLEAUX, &len);
    if (r != 1 && r != 0)
        err_fatal(-1, "erreur lors du chargement des donnes de cartes", COMMANDE_SUPPRIMER_CARTE);
    
    printf("Id de carte: ");
    int id;
    if (!maybe_scanf_int(&id, COMMANDE_SUPPRIMER_CARTE)) {
        printf("Operation annulee.\n");
        return;
    }
    
    char exists;
    int index = indice_carte(t_carte_ids, len, id, &exists);
    
    if (!exists) {
        err_normal("Il n'y a pas de carte avec cet ID.", COMMANDE_SUPPRIMER_CARTE);
    } else if (t_carte_act[index]) {
        err_normal("La carte est toujours active, elle ne peut pas etre supprimee", COMMANDE_SUPPRIMER_CARTE);
    } else {
        {
            int ids[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
            char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM], adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];
            
            int increment, adh_len;
            charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &adh_len, ids, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
            
            int adh_idx = indice_adherent(ids, adh_len, t_carte_adh[index], &exists);
            
            if (exists && idcarte[adh_idx] == id) {
                err_normal("Impossible de supprimer cette carte car elle est actuellement utilisee par son adherent.\nVous devez dabord changer de carte ou supprimer l'adherent.", COMMANDE_SUPPRIMER_CARTE);
                return;
            }
        }
        
        t_carte_remove(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, &len, index);
        
        sauvegarder_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, len);
    }
}

float calcul_montant_point(int pts) {
    return (float) pts * 0.5; 
}

void commande_recharger_carte(void) {
    char t_carte_act[TAILLE_TABLEAUX];
    int t_carte_ids[TAILLE_TABLEAUX], t_carte_adh[TAILLE_TABLEAUX], t_carte_pts[TAILLE_TABLEAUX], len;
    int r = charger_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, TAILLE_TABLEAUX, &len);
    if (r != 1 && r != 0)
        err_fatal(-1, "erreur lors du chargement des donnes de cartes", COMMANDE_RECHARGER_CARTE);
    
    printf("Id de carte: ");
    int id;
    if (!maybe_scanf_int(&id, COMMANDE_RECHARGER_CARTE)) {
        printf("Operation annulee.\n");
        return;
    }
    
    char exists;
    int index = indice_carte(t_carte_ids, len, id, &exists);
    
    if (exists) {
        if (!t_carte_act[index]) {
            err_normal("Carte non activee.", COMMANDE_RECHARGER_CARTE);
            return;
        }
        
        printf("Nombre de points a ajouter: ");
        int to_add;
        int r = scanf("%d%*c", &to_add);
        if (r == 1) {
            float montant = calcul_montant_point(to_add);
            printf("Montant a debiter: %2.2f euro\n\n", montant);
            if (ask("Valider ?")) {
                t_carte_pts[index] += to_add;
                
                sauvegarder_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, len);
            }
        } else if (r == EOF) {
            return;
        } else {
            err_fatal(-1, "erreur lors de la lecture du numbre de points", COMMANDE_RECHARGER_CARTE);
        }
    } else {
        err_normal("Il n'y a pas de carte avec cet ID.", COMMANDE_RECHARGER_CARTE);
    }
}

void commande_activer_carte(void) {
    char t_carte_act[TAILLE_TABLEAUX];
    int t_carte_ids[TAILLE_TABLEAUX], t_carte_adh[TAILLE_TABLEAUX], t_carte_pts[TAILLE_TABLEAUX], len;
    int r = charger_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, TAILLE_TABLEAUX, &len);
    if (r != 1 && r != 0)
        err_fatal(-1, "erreur lors du chargement des donnes de cartes", COMMANDE_ACTIVER_CARTE);
    
    printf("Id de carte: ");
    int id_carte;
    if (!maybe_scanf_int(&id_carte, COMMANDE_ACTIVER_CARTE)) {
        printf("Operation annulee.\n");
        return;
    }
    
    char exists;
    int index_carte = indice_carte(t_carte_ids, len, id_carte, &exists);
    
    if (exists) {
        if (t_carte_act[index_carte]) {
            printf("Cette carte est deja activee.\n");
        } else { 
            int ids[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
            char noms[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM], adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];
            
            int increment, adh_len;
            charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &adh_len, ids, noms, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
            
            // désactiver l'ancienne carte de l'adhérent
            int adh_index = indice_adherent(ids, adh_len, t_carte_adh[index_carte], &exists);
            if (exists) {
                if (idcarte[adh_index] != PAS_DE_CARTE) {
                    int ancienne_carte = indice_carte(t_carte_ids, len, idcarte[adh_index], &exists);
                    
                    if (exists)
                        t_carte_adh[ancienne_carte] = 0 /* FALSE */;
                }
                
                t_carte_act[index_carte] = 1/*TRUE*/;
                idcarte[adh_index] = id_carte;
                
                sauvegarder_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, len);
                sauvegarde_fichier_adherant(increment, adh_len, ids, noms, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
            }
        }
    } else {
        err_normal("Il n'y a pas de carte avec cet ID.", COMMANDE_ACTIVER_CARTE);
    }
}

void commande_desactiver_carte(void) {
    char t_carte_act[TAILLE_TABLEAUX];
    int t_carte_ids[TAILLE_TABLEAUX], t_carte_adh[TAILLE_TABLEAUX], t_carte_pts[TAILLE_TABLEAUX], len;
    int r = charger_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, TAILLE_TABLEAUX, &len);
    if (r != 1 && r != 0)
        err_fatal(-1, "erreur lors du chargement des donnes de cartes", COMMANDE_DESACTIVER_CARTE);
    
    printf("Id de carte: ");
    int id;
    if (!maybe_scanf_int(&id, COMMANDE_DESACTIVER_CARTE)) {
        printf("Operation annulee.\n");
        return;
    }
    
    char exists;
    int index = indice_carte(t_carte_ids, len, id, &exists);
    
    if (exists) {
        if (!t_carte_act[index]) {
            printf("Cette carte est deja desactivee.\n");
        } else {
            t_carte_act[index] = 0/*TRUE*/;
            
            sauvegarder_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, len);
        }
    } else {
        err_normal("Il n'y a pas de carte avec cet ID.", COMMANDE_DESACTIVER_CARTE);
    }
}

char charger_cartes(int t_carte_ids[], char t_carte_act[], int t_carte_adh[], int t_carte_pts[], int capacite, int *lon) {
    const char *CONTEXT = "charger donnees cartes";
    
    FILE *f = fopen(FICHIER_CARTES, "r");
    if (f == NULL)
        err_fatal(-1, "erreur lors de l'ouverture en lecture du fichier de cartes", CONTEXT);

    *lon = 0;

    while (1)
    {
        int id, act, adh_id, pts;
        if (fscanf(f, "%d %d %d %d;", &id, &act, &adh_id, &pts) != 4)
        {
            if (feof(f))
            {
                // Cas ou il restait des espaces à la fin du fichier
                break;
            }

            fclose(f);

            // On considère qu'il s'agit d'une erreur grave
            err_fatal(-1, "fichier mal formatte", CONTEXT);
        }

        if (*lon >= capacite)
        {
            // Capacité maximale atteinte
            fclose(f);

            return 1;
        }

        t_carte_ids[*lon] = id;
        t_carte_act[*lon] = act != 0;
        t_carte_adh[*lon] = adh_id;
        t_carte_pts[*lon] = pts;
        *lon += 1;
    }

    fclose(f);
    return 0; // Succes!
}

void sauvegarder_cartes(const int t_carte_ids[], const char t_carte_act[], const int t_carte_adh[], const int t_carte_pts[], int lon) {
    const char *CONTEXT = "sauvegarder donnees cartes";
    
    FILE *f = fopen(FICHIER_CARTES, "w");
    if (f == NULL)
        err_fatal(-1, "erreur lors de l'ouverture en ecriture du fichier de cartes", CONTEXT);

    int i;
    for (i = 0; i < lon; i++)
    {
        if (fprintf(f, "%d %d %d %d;\n", t_carte_ids[i], (int) t_carte_act[i], t_carte_adh[i], t_carte_pts[i]) < 0)
        {
            fclose(f);

            err_fatal(-1, "erreur lors de l'ecriture dans le fichier", CONTEXT);
        }
    }

    fclose(f);
}

int trouver_prochain_uid_libre(const int t_carte_ids[], int len) {
    int id = 0, i;
    for (i = 0; i < len; i++) {
        if (id <= t_carte_ids[i]) {
            id = t_carte_ids[i] + 1;
        }
    }
    
    return id;
}

int indice_carte(const int t_carte_ids[], int len, int id, char *found) {
    int i;
    
    *found = 0;
    for (i = 0; i < len; i++) {
        if (t_carte_ids[i] >= id) {
            *found = t_carte_ids[i] == id;
            break;
        }
    }
    
    return i;
}

void t_carte_insert(int t_carte_ids[], char t_carte_act[], int t_carte_adh[], int t_carte_pts[], /* int capacite, */ int *len, int index, int new_id, char new_act, int new_adh, int new_pts) {
    int i;
    for (i = *len; i > index; i--) {
        t_carte_ids[i] = t_carte_ids[i - 1];
        t_carte_act[i] = t_carte_act[i - 1];
        t_carte_adh[i] = t_carte_adh[i - 1];
        t_carte_pts[i] = t_carte_pts[i - 1];
    }
    
    t_carte_ids[index] = new_id;
    t_carte_act[index] = new_act;
    t_carte_adh[index] = new_adh;
    t_carte_pts[index] = new_pts;
    *len += 1;
}

void t_carte_remove(int t_carte_ids[], char t_carte_act[], int t_carte_adh[], int t_carte_pts[], int *len, int index) {
    if (!*len) return;
    
    *len -= 1;
    
    int i;
    for (i = index; i < *len; i++) {
        t_carte_ids[i] = t_carte_ids[i + 1];
        t_carte_act[i] = t_carte_act[i + 1];
        t_carte_adh[i] = t_carte_adh[i + 1];
        t_carte_pts[i] = t_carte_pts[i + 1];
    }
}
