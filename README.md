# Projet SAE1.01 - Implementation
Un programme de gestion des d'un complexe sportif.

Vous pouvez trouver le cahier des charges [ici](CDC.md) ainsi que la table des code d'erreurs [ici](table_error.md).

Dépot codefist:
[https://codefirst.iut.uca.fr/git/mathis.delage/SAE1.01](https://codefirst.iut.uca.fr/git/mathis.delage/SAE1.01)

## Autheurs
 *  Mathis Moulin
 *  Mathis Delage
 *  Mathieu Grousseau

## Compiler
Pour compiler le projet, tapez `make` dans le dossier racine du projet.

## Documentation
Pour générer la documentation, tapez `make doc` dans le dossier racine du projet.
La documentation générée est rangée dans le répertoire `doc`.

## Nettoyer
Pour nettoyer les fichier générés, tapez `make clean` dans le dossier racine du projet.