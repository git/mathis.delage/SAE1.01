#include <stdio.h>
#include "commun.h"
#include "carte.h"
#include "activite.h"
#include "adherant.h"

const char *PATH_ACT_DECOUV = "data/decouverte";
const char *PATH_ACT_STATS = "data/statistiques";

void affiche_banniere(void);

void commande_afficher_aide(void);
void commande_participer(void);
void commande_afficher_statistiques(void);

int main(void) {
    affiche_banniere();
    
    while (/* !feof(stdin) */ 1) {
        printf("> ");
        int o;
        int r = scanf("%d%*c", &o);
        
        if (r == EOF) {
            break;
        } else if (r != 1) {
            err_fatal(-1, "erreur de lecture de l'entree standard", NULL);
        }
        
        if (o == 1) {
            commande_afficher_aide();
        } else if (o == 2) {
            commande_affiche_tous_adherents();
        } else if (o == 3) {
            commande_affiche_adherent();
        } else if (o == 4) {
            commande_infos_adherent();
        } else if (o == 5) {
            commande_creer_adherent();
        } else if (o == 6) {
            commande_maj_adherent();
        } else if (o == 7) {
            commande_suppimer_adherent();
        } else if (o == 8) {
            commande_recharger_carte();
        } else if (o == 9) {
            commande_ajouter_carte();
        } else if (o == 10) {
            commande_activer_carte();
        } else if (o == 11) {
            commande_desactiver_carte();
        } else if (o == 12) {
            commande_supprimer_carte();
        } else if (o == 13) {
            commande_afficher_statistiques();
        } else if (o == 14) {
            commande_list_act();
        } else if (o == 15) {
            commande_exist_act();
        } else if (o == 16) {
            commande_ajout_activite();
        } else if (o == 17) {
            commande_modif_point();
        } else if (o == 18) {
            commande_suppr_act();
        } else if (o == 19) {
            commande_participer();
        } else if (o == 20) {
            if (ask("Cloturer la journee ? (Reinitializer les statistiques)")) {
                reset_stats(PATH_ACT_STATS);
            }
            
            break;
        } else {
            err_normal("option inconnue\nTapez 1 pour affichier l'aide.\n", NULL);
        }
    }

    return 0;
}

void affiche_banniere(void) {
    printf("+---------------------------+\n");
    printf("|            SAE            |\n");
    printf("| Tapez 1 pour avoir l'aide |\n");
    printf("+---------------------------+\n");
}

void commande_afficher_aide(void) {
    int i = 0;
    printf("%2d)\tafficher l'aide\n", ++i);
    printf("%2d)\tafficher tous les adherents\n", ++i);
    printf("%2d)\tafficher un adherent\n", ++i);
    printf("%2d)\tafficher les infos d'un adherent\n", ++i);
    printf("%2d)\tcreer un adherent\n", ++i);
    printf("%2d)\tmettre a jour un adherent\n", ++i);
    printf("%2d)\tsupprimer un adherent\n", ++i);
    printf("%2d)\t%s\n", ++i, COMMANDE_RECHARGER_CARTE);
    printf("%2d)\t%s\n", ++i, COMMANDE_AJOUTER_CARTE);
    printf("%2d)\t%s\n", ++i, COMMANDE_ACTIVER_CARTE);
    printf("%2d)\t%s\n", ++i, COMMANDE_DESACTIVER_CARTE);
    printf("%2d)\t%s\n", ++i, COMMANDE_SUPPRIMER_CARTE);
    printf("%2d)\tstatistiques\n", ++i);
    printf("%2d)\tlister les activites\n", ++i);
    printf("%2d)\texistance de l'activite\n", ++i);
    printf("%2d)\tajouter une activite\n", ++i);
    printf("%2d)\tchanger le nombre de points d'une activite\n", ++i);
    printf("%2d)\tsupprimer une activite\n", ++i);
    printf("%2d)\tparticier a une activite\n", ++i);
    printf("%2d)\tquitter\n", ++i);
}

void commande_participer(void) {
    printf("Identifiant de l'adherent: ");
    int adherent;
    if (!maybe_scanf_int(&adherent, "particier a une activite")) {
        printf("Operation annulee.\n");
        return;
    }
    
    // scanf("%*c");
    
    int increment, longueur;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];
    
    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
    
    char trouve;
    int index_adh = indice_adherent(id, longueur, adherent, &trouve);
    
    if (!trouve) {
        err_normal("Il n'y a pas d'adherent avec cet identifiant.", "particier a une activite");
        return;
    }
    
    char nom_act[MAX_SIZE_ACT];
    demande_nom(nom_act);
    
    int position;
    int r = activite_exist(nom_act, &position);
    if (r == -1) {
        err_fatal(ERR_CODE_FAIL_FICHIER, "erreur chargement des activites", "particier a une activite");
    } else if (r == 0) {
        err_normal("il n'y a pas d'activite portant ce nom", "particier a une activite");
        return;
    }
    
    int list_pts[MAX_ACT];
    char list_act[MAX_ACT][MAX_SIZE_ACT];
    charger_act(list_act, list_pts, MAX_ACT);
    
    if (get_stat(PATH_ACT_STATS, adherent, nom_act) > 0) {
        err_normal("Cet adherent a deja fait cette activite aujourd'hui.", "particier a une activite");
        return;
    }
        
    // TODO: possible desync des index entre les chargements...
    
    int debite = list_pts[position];
    
    char decouverte = get_stat(PATH_ACT_DECOUV, adherent, nom_act) == 0;
    if (decouverte) {
        debite /= 2;
    }
    
    char t_carte_act[TAILLE_TABLEAUX];
    int t_carte_ids[TAILLE_TABLEAUX], t_carte_adh[TAILLE_TABLEAUX], t_carte_pts[TAILLE_TABLEAUX], len;
    r = charger_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, TAILLE_TABLEAUX, &len);
    if (r != 1 && r != 0)
        err_fatal(-1, "erreur lors du chargement des donnes de cartes", "particier a une activite");
    
    if (idcarte[index_adh] == PAS_DE_CARTE) {
        err_normal("Cet adherent n'a pas encore de carte.", "particier a une activite");
        return;
    }
    
    int carte_idx = indice_carte(t_carte_ids, len, idcarte[index_adh], &trouve);
    if (!trouve) {
        err_fatal(-1, "Carte inconnue", "particier a une activite");
    }
    
    if (!t_carte_act[carte_idx]) {
        err_normal("Carte desactivee.", "particier a une activite");
        return;
    }
    
    if (t_carte_pts[carte_idx] < debite) {
        err_normal("Il n'y a pas assez de points sur la carte.", "particier a une activite");
        return;
    }
    
    if (decouverte)
        printf("C'est la premiere seance, tarification a 50%%...\n");
    
    if (ask("Valider ?")) {
        if (decouverte)
            increment_stat(PATH_ACT_DECOUV, adherent, nom_act);
        
        t_carte_pts[carte_idx] -= debite;
        sauvegarder_cartes(t_carte_ids, t_carte_act, t_carte_adh, t_carte_pts, len);
        
        increment_stat(PATH_ACT_STATS, adherent, nom_act);
    }
}

void commande_afficher_statistiques(void) {
    char t_act_nom[MAX_ACT][MAX_SIZE_ACT];
    int t_act_size[MAX_ACT], t_act_adh[MAX_ACT][MAX_STATS_ENTRY_SIZE], t_act_cnt[MAX_ACT][MAX_STATS_ENTRY_SIZE];
    int len;
    
    charge_stats_generic(PATH_ACT_STATS, MAX_ACT, &len, t_act_nom, t_act_size, t_act_adh, t_act_cnt);
    
    printf("Statistiques de la journeé:\n");
    printf("+======================+======================+\n");
    printf("| %20s | %20s |\n", "Activite", "Adherents");
    printf("+======================+======================+\n");
    
    int i;
    for (i = 0; i < len; i++) {
        int total = 0;
        
        int j;
        for (j = 0; j < t_act_size[i]; j++) {
            total += t_act_cnt[i][j];
        }
        
        printf("| %20s | %20d |\n", t_act_nom[i], total);
    }
    printf("+======================+======================+\n\n");
}
