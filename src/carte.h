/**
 * \file carte.h
 * \brief Module concernant les cartes des adhérents
 * \author Grousseau Mathieu
 * 
 * Ce fichier regroupe les commandes concernant les cartes ainsi que des 
 * fonctions pour manipuler les données des cartes depuis d'autres modules.
 */

/* Noms constants pour les commandes
    Pour garder de la consistance entre les noms dans le menu et les messages d'erreur */
// const char *COMMANDE_AJOUTER_CARTE dans l'idéal mais problèmes de définition multiple
// et on n'a pas vu les macro #ifdef ...
// donc on utilise des macro #define a la place
#define TAILLE_TABLEAUX 200

#define COMMANDE_AJOUTER_CARTE "creer une carte"
#define COMMANDE_SUPPRIMER_CARTE "detruire une carte"
#define COMMANDE_ACTIVER_CARTE "activer une carte"
#define COMMANDE_DESACTIVER_CARTE "desactiver une carte"
#define COMMANDE_RECHARGER_CARTE "alimenter une carte"


/**
 * \brief (Crée) et ajoute une carte
 *
 * Cette commande demande à quel adhérant la carte doit être attribuée et 
 * indique l'identifiant de la carte nouvellement crée.
 * Les cartes ne sont pas actives par défaut.
 */
void commande_ajouter_carte(void);

/**
 * \brief Supprime une carte
 *
 * Cette commande demande l'identifiant de la carte et la supprime.
 * Pour que la carte soit supprimée elle doit d'abord être désactivée.
 */
void commande_supprimer_carte(void);

/**
 * \brief Active une carte existante.
 *
 * Cette commande demande l'identifiant de la carte et l'active.
 * Cela désactivera l'ancienne carte de d'adhérent, si il en avait une.
 */
void commande_activer_carte(void);

/**
 * \brief Désactive une carte existante.
 *
 * Cette commande demande l'identifiant de la carte et la désactive.
 */
void commande_desactiver_carte(void);

/**
 * \brief Rajoute des points a une carte active existante.
 *
 * Cette commande demande l'identifiant de la carte,
 * demande le nombre de points à rajouter,
 * annonce le prix à débiter
 * et ajoute les points après validation.
 */
void commande_recharger_carte(void);

/**
 * \brief Recherche l'index correspondant à un identifiant dans le tableau
 * \param [in] t_carte_ids La colonne des identifiants
 * \param len La taille du tableau
 * \param id L'identifiant duquel l'index est recherché
 * \param [out] found Si la valeur est présente dans le tableau
 * 
 * Cette fonction recherche la position correspondante à l'identifiant donné.
 * Si l'identifiant ne se trouve pas dans la table, l'index d'insertion est 
 * donné.
 * Cette fonction suppose que le tableau est trié par identifiants de cartes
 * croissant.
 */
int indice_carte(const int t_carte_ids[], int len, int id, char *found);

/**
 * \brief Charge les données des cartes depuis le disque.
 *
 * \param [out] t_carte_ids Tableau des identifiants uniques des cartes
 * \param [out] t_carte_act Tableau des états d'activation des cartes
 * \param [out] t_carte_adh Tableau des identifiants des adhérents par carte
 * \param [out] t_carte_pts Tableau des points par carte
 * \param capacite La capacité maximale des tableaux
 * \param [in,out] lon Le nombre (la taille) d'éléments chargés dans le tableau
 *
 * \return 0 si chargé avec succès, 1 si la capacité maximale est atteinte
 */
char charger_cartes(int t_carte_ids[], char t_carte_act[], int t_carte_adh[], int t_carte_pts[], int capacite, int *lon);

/**
 * \brief Sauvegarde les données des cartes sur le disque
 *
 * \param [in] t_carte_ids Tableau des identifiants uniques des cartes
 * \param [in] t_carte_act Tableau des états d'activation des cartes
 * \param [in] t_carte_adh Tableau des identifiants des adhérents par carte
 * \param [in] t_carte_pts Tableau des points par carte
 * \param lon Le nombre (la taille) d'éléments présents dans le tableau
 */
void sauvegarder_cartes(const int t_carte_ids[], const char t_carte_act[], const int t_carte_adh[], const int t_carte_pts[], int lon);
