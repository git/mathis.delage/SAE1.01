/**
 * \file activite.h
 * \brief Module concernant les activités du complexe
 * \author DELAGE Mathis
 *
 * Ce fichier regroupe les commandes concernant les activités ainsi que des
 * fonctions pour manipuler les données des activités depuis d'autres modules.
 */

/* Noms constants pour les commandes
    Pour garder de la consistance entre les noms dans le menu et les messages d'erreur */

// define de la taille maximale du nom des activités et du maximum d'activités
#define MAX_SIZE_ACT 20
#define MAX_ACT 100
#define MAX_STATS_ENTRY_SIZE 100


/**
 * \brief Demande le nom en entrée standard
 *
 * \param [out] nom_act Donne le nom saisi
 */
void demande_nom(char *nom_act);

/**
 * \brief Demande un nombre de point en entrée standard
 *
 * \param [out] points Donne le nombre de points saisie
 */
void demande_point(int *points);

/**
 * \brief Test si une activité existe
 *
 * \param [in] nom_act Nom de l'activité à rechercher
 * \param [out] position Donne la position à laquelle à été trouvé l'activité si elle a été trouvé
 *
 * \return 1 si trouvé, 0 si pas trouvé et -1 s'il y a eu une erreur lors de l'ouverture/lecture du fichier data/activité
 */
int activite_exist(char *nom_act, int *position);

/**
 * \brief Obtient les activités du fichier activités
 *
 * \param TMAX Taille maximale de la liste des activités
 * \param list_act Liste dans la quelle les activités sont enregistré
 * \param list_pts Liste dans la quelle les points sont enregistré
 *
 * \return la taille logique des listes après les avoir remplies
 */
int charger_act(char list_act[][MAX_SIZE_ACT], int list_pts[], int TMAX);

/**
 * \brief Enregistre les activités dans le fichier data/activités
 *
 * \param list_act Liste des activités
 * \param list_pts Liste des points au même index que les activités
 * \param Tlog Donne la taille logique du tableau d'activité
 */
void save_act(char list_act[][MAX_SIZE_ACT], int list_pts[], int Tlog);

/**
 * \brief Listes toutes les activités enregistrées
 */
void commande_list_act(void);

/**
 * \brief Commande pour voir si une activité existe
 *
 * Demande le nom de l'activité
 * Affiche si l'activité a été trouvé la ligne à laquelle elle se situe
 * Sinon affiche qu'elle n'a pas été trouvé
 */
void commande_exist_act(void);

/**
 * \brief Commande pour ajouter une activité
 *
 * Fonctionne grâce à 'ajout_activite'
 */
void commande_ajout_activite(void);

/**
 * \brief Ajoute une activité au fichier data/activite
 *
 * \param [in] nom_act Donne le nom de l'activité à ajouter
 * \param nb_point Donne le nombre de point de l'activité
 */
void ajout_activite(char *nom_act, int nb_point);

/**
 * \brief Change le nombre de point d'une activité existante
 *
 *  Fonctionne grâce à 'modif_point'
 */
void commande_modif_point(void);

/**
 * \brief Modifie le nombre de point d'une activité existante
 *
 * \param [in] nom_act Donne le nom de l'activité à modifier
 * \param nv_point Donne le nouveau nombre de point de l'activité
 */
void modif_point(char *nom_act, int nv_point);

/**
 * \brief Supprime une activité
 *
 * Demande une confirmation avant la suppression
 * Fonctionne grâce à 'suppr_act'
 */
void commande_suppr_act();

/**
 * \brief Supprime une activité existante
 *
 * \param [in] nom_act Donne le nom de l'activité à supprimer
 */
void suppr_act(char *nom_act);

/**
 * \brief Charge les stats de la journée
 * 
 * \param [in] fichier Nom du fichier à ouvrir
 * \param t_capacity Cpacité des tableaux
 * \param [out] t_length Taille logique du tableau
 * \param t_act_nom Tableau des activités
 * \param t_act_size Taille du tableau dans le tableau
 * \param t_act_adh Tableau des id d'adhérent par activité
 * \param t_act_cnt Tableau du nombre de fois qu'un adhérent a participé à une activités
*/
void charge_stats_generic(const char *fichier, int t_capacity, int *t_length, char t_act_nom[][MAX_SIZE_ACT], int t_act_size[], int t_act_adh[][MAX_STATS_ENTRY_SIZE], int t_act_cnt[][MAX_STATS_ENTRY_SIZE]);

/**
 * \brief increment les stats de participation d'un adhérent a ue activité
 * \param fichier le fichier de stats
 * \param adherent l'identifiant de l'adhérent
 * \param [in] activite le nom de l'activité
 */
void increment_stat(const char *fichier, int adherent, const char *activite);

/**
 * \brief recupère le nombre de fois qu'un adhérent à participé à une activité spécifiée
 * \param fichier le fichier de stats
 * \param adherent l'identifiant de l'adhérent
 * \param activite le nom de l'activité
 * \return le nombre de participation
 */
int get_stat(const char *fichier, int adherent, const char *activite);

/**
 * \brief remet a zéro le fichier statistique
 * \param [in] fichier le fichier de stats
 */
void reset_stats(const char *fichier);
