# Table des erreurs

| N° Erreur fatal |Description erreurs              |
|-----------------|---------------------------------|
|       1         | Ouverture du fichier impossible |
|       2         | Tableau trop petit              |
|       3         | Erreur dans l'écriture          |
|       4         | Erreur dans la lecture          |
