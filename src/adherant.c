#include "adherant.h"

const char *FICHIER_ADHERENT = "data/adherant.txt";

/**
 * \brief Test si il y a des espaces.
 * \param [in] chaine la chaîne de caractère à tester.
 * \return 0 si il n'y a pas d'espace.
 * \return 1 si il y a un ou plusieurs espaces.
 */
char test_espace(char chaine[]);

/**
 * \brief Remplace les espaces par des underscores.
 * \param [in] chaine la chaîne de caractère à tester.
 */
void replace_espace_par_underscore(char chaine[]);

/**
 * \brief Vérifie si un id d'adhérent n'existe pas.
 * \param [in] id_adher l'id adhérent à tester.
 * \return 0 si l'id est trouvé -1 sinon.
 */
int not_exist(int id_adher);

/**
 * \brief Supprime une ligne dans le fichier 'adherant.txt'
 * \param [in out] longueur Pointeur sur un entier qui permet de modifie la taille des lignes d'adhérent du fichier 'adherant.txt'
 * (dans ce cas ci il va diminuer de 1).
 * \param [out] index l'index de la ligne à supprimer.
 * \param [out] id Tableau des identifiants uniques des adhérents.
 * \param [out] nom Tableau des nom des adhérents.
 * \param [out] prenom Tableau des prénoms des adhérents.
 * \param [out] idcarte Tableau des idcartes uniques des adhérents.
 * \param [out] adresse_email Tableau des adresses emails des adhérents.
 * \param [out] adresse Tableau des adresses des adhérents.
 * \param [out] code_postal Tableau des codes postals des adhérents.
 * \param [out] num_telephone Tableau des numéros de téléphones des adhérents.
 */
void supprimer_ligne(int * longueur, int index, int id[],char nom[][TAILLE_MAX_NOM],char prenom[][TAILLE_MAX_PRENOM], int idcarte[], char adresse_email[][TAILLE_MAX_ADRESSE_EMAIL], char adresse[][TAILLE_MAX_ADRESSE], int code_postal[], int num_telephone[]);

/**
 * \brief Demande l'id d'un adhérent pour ensuite l'utiliser dans les fonctions voulu. 
 * \return Retoune l'id récuperé dans l'entrée standard.
 */
int demande_id_adherent(void);



void commande_affiche_adherent(void){
    // Charge le fichier 'adherant.txt' dans des tableaux. 
    int increment, longueur, id_adher;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];

    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    // Demande l'id voulut.
    id_adher = demande_id_adherent();

    // Test si l'id_adher existe.
    if (not_exist(id_adher)) {
        err_normal("Probleme lors de la recherche de l'id.", "id adherent inexistant.");
        return;
    }
    
    // Affiche les données de l'adhérant recherché.
    printf("L'identifiant %d correspond a %s %s avec l'identifiant de carte %d.\n", id[id_adher], nom[id_adher], prenom[id_adher], idcarte[id_adher]);
}

void commande_affiche_tous_adherents(void){
    // Charge le fichier 'adherant.txt' dans des tableaux. 
    int increment, longueur;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];

    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    printf("---------------------------------------------------------------------------------------------------\n");

    int i = 0;
    while (i < longueur){
        // Boucle pour afficher tout les adhérants.
        
        printf("--> L'identifiant %d correspond a %s %s avec l'identifiant de carte %d.\n", id[i], nom[i], prenom[i], idcarte[i]);
        i++;
    }

    printf("---------------------------------------------------------------------------------------------------\n");

}

void commande_infos_adherent(void){ 
    // Charge le fichier 'adherant.txt' dans des tableaux. 
    int increment, longueur, id_adher;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];

    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    // Demande l'id voulut.
    id_adher = demande_id_adherent();

    // Test si l'id_adher existe.
    if (not_exist(id_adher)){
        err_normal("Probleme lors de la recherche de l'id.", "id adherent inexistant.");
        return;
    }

    // Affiche les données de l'adhérant recherché.
    printf("L'identifiant %d correspond a %s %s avec l'identifiant de carte %d.\n", id[id_adher], nom[id_adher], prenom[id_adher], idcarte[id_adher]);
    printf("Email: %s.\n",adresse_email[id_adher]);
    printf("Adresse: %s.\n",adresse[id_adher]);
    printf("Code postal: %d.\n", code_postal[id_adher]);
    printf("Numero de telephone: %d.\n", num_telephone[id_adher]);
}

void commande_creer_adherent(void){
    // Charge le fichier 'adherant.txt' dans des tableaux. 
    int increment, longueur;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];

    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    // Ajout le nouvel id + idcarte à l'adhérant (defini à -1).
    id[longueur] = increment;
    idcarte[longueur] = PAS_DE_CARTE;

    // Demande les informations pour le nouvel adhérent.
    printf("Veuillez saisir le nom du nouvel arrivant: \n");
    fgets(nom[longueur], TAILLE_MAX_NOM, stdin);
    suppr_retour_a_la_ligne(nom[longueur]);
    replace_espace_par_underscore(nom[longueur]);

    printf("Veuillez saisir le prenom du nouvel arrivant: \n");
    fgets(prenom[longueur], TAILLE_MAX_PRENOM, stdin);
    suppr_retour_a_la_ligne(prenom[longueur]);
    replace_espace_par_underscore(prenom[longueur]);

    printf("Veuillez saisir l'email du nouvel arrivant: \n");
    while (1) {
        fgets(adresse_email[longueur], TAILLE_MAX_ADRESSE_EMAIL, stdin);
        suppr_retour_a_la_ligne(adresse_email[longueur]);
        if (test_espace(adresse_email[longueur])) {
            err_normal("Probleme d'espace\n",NULL);
            printf("Veuillez re-saisir\n");
        } else
            break;
    }

    printf("Veuillez saisir le code postal du nouvel arrivant: \n");
    scanf("%d%*c", &code_postal[longueur]);

    printf("Veuillez saisir l'adresse du nouvel arrivant: \n");
    fgets(adresse[longueur], TAILLE_MAX_ADRESSE, stdin);
    suppr_retour_a_la_ligne(adresse[longueur]);
    replace_espace_par_underscore(adresse[longueur]);

    printf("Veuillez saisir le numero de telephone du nouvel arrivant: \n");
    scanf("%d",&num_telephone[longueur]);

    // Incrémentation des variables increment et longueur.
    increment++;
    longueur++;
    
    // Ajout du nouvel adhérent dans le fichier 'adherant.txt'.
    sauvegarde_fichier_adherant(increment, longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
}

void commande_maj_adherent(void){
    // Charge le fichier 'adherant.txt' dans des tableaux. 
    int increment, longueur, id_adher;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];

    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    // Demande l'id voulut.
    id_adher = demande_id_adherent();

    // Test si l'id_adher existe.
    if (not_exist(id_adher)){
        err_normal("Probleme lors de la recherche de l'id.", "id adherent inexistant.");
        return;
    }

    // Affiche les options de modifications.
    int choix = 0;

    while(1){
        printf("---------------------------------------------------------------------------------------------------\n");
        printf("Que voulez-vous mettre a jour ?\n");
        printf("1) Mise a jour nom :\n");
        printf("2) Mise a jour prenom :\n");
        printf("3) Mise a jour adresse email :\n");
        printf("4) Mise a jour adresse :\n");
        printf("5) Mise a jour code postal :\n");
        printf("6) Mise a jour numero de telephone :\n");
        printf("0)Quittez :\n");
        printf("---------------------------------------------------------------------------------------------------\n");

        scanf("%d%*c", &choix);
        
        if (choix==1){

            // Saisie nom
            printf("Veuillez saisir le nouveau nom :\n");
            fgets(nom[id_adher], TAILLE_MAX_NOM, stdin);
            suppr_retour_a_la_ligne(nom[id_adher]);
            replace_espace_par_underscore(nom[id_adher]);
            printf("Changement bien effectue !\n");
        } else if (choix==2){

            // Saisie prénom
            printf("Veuillez saisir le nouveau prenom :\n");
            fgets(prenom[id_adher], TAILLE_MAX_PRENOM, stdin);
            suppr_retour_a_la_ligne(prenom[id_adher]);
            replace_espace_par_underscore(prenom[id_adher]);
            printf("Changement bien effectue !\n");
        } else if (choix==3){

            // Saisie adresse email
            printf("Veuillez saisir la nouvelle adresse email :\n");
            while (1) {
                fgets(adresse_email[id_adher], TAILLE_MAX_ADRESSE_EMAIL, stdin);
                suppr_retour_a_la_ligne(adresse_email[id_adher]);
                if (test_espace(adresse_email[id_adher])) {
                    err_normal("Probleme d'espace\n",NULL);
                    printf("Veuillez re-saisir\n");
                } else
                    break;
            }
            printf("Changement bien effectue !\n");
        } else if (choix==4){

            // Saisie adresse
            printf("Veuillez saisir la nouvelle adresse :\n");
            fgets(adresse[id_adher], TAILLE_MAX_ADRESSE, stdin);
            suppr_retour_a_la_ligne(adresse[id_adher]);
            replace_espace_par_underscore(adresse[id_adher]);
            printf("Changement bien effectue !\n");
        } else if (choix==5){

            // Saisie code postal
            printf("Veuillez saisir le nouveau code postal :\n");
            scanf("%d", &code_postal[id_adher]);
            printf("Changement bien effectue !\n");
        } else if (choix==6){

            // Saisie numéro de téléphone
            printf("Veuillez saisir le nouveau numero de telephone :\n");
            scanf("%d", &num_telephone[id_adher]);
            printf("Changement bien effectue !\n");
        } else if (choix==0){

            // Quitte la boucle
            break;
        }
    }

    // Ajout du nouvel adhérent dans le fichier 'adherant.txt'.
    sauvegarde_fichier_adherant(increment, longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
}

void commande_suppimer_adherent(void){
    // Charge le fichier 'adherant.txt' dans des tableaux. 
    int increment, longueur, id_adher;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];

    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    // Demande l'id voulut.
    id_adher = demande_id_adherent();

    // Test si l'id_adher existe.
    if (not_exist(id_adher)){
        err_normal("Probleme lors de la recherche de l'id.", "id adherent inexistant.");
        return;
    }

    // Supprime l'adhérent.
    supprimer_ligne(&longueur, id_adher, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    // Ajout du nouvel adhérent dans le fichier 'adherant.txt'.
    sauvegarde_fichier_adherant(increment, longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);
}

int indice_adherent(const int ids[], int longueur, int id, char *trouve) {
    int indice;
    
    *trouve = 0; // on initialise la variable
    for (indice = 0; indice < longueur; indice++) {
        if (ids[indice] >= id) {
            *trouve = ids[indice] == id;
            break;
        }
    }
    
    return indice;
}

void charger_fichier_adherant(int *increment, int capacite, int *longueur, int id[], char nom[][TAILLE_MAX_NOM], char prenom[][TAILLE_MAX_PRENOM], int idcarte[], char adresse_email[][TAILLE_MAX_ADRESSE_EMAIL], char adresse[][TAILLE_MAX_ADRESSE], int code_postal[], int num_telephone[]) {
    // Charge tous les lignes du fichier 'adherant.txt'.
    FILE * f=fopen(FICHIER_ADHERENT,"r");
    int i = 0;

    if (f==NULL){

        // Test le chargement du fichier.
        err_fatal(ERR_CODE_FAIL_FICHIER, "Erreur de chargement fichier",  "consernant le fichier 'adhérant.txt'.");
    }

    fscanf(f,"%d", increment);
    *longueur = 0;

    while(1) {
        if (*longueur >= capacite) {
            fclose(f);
            err_fatal(ERR_CODE_TABLE_SATUREE, "tableau trop petit/problème tableau", "consernant le fichier 'adhérant.txt'.");
        }
        
        fscanf(f,"%d %s %s %d %s %s %d %d", &id[i], nom[i], prenom[i], &idcarte[i], adresse_email[i], adresse[i], &code_postal[i], &num_telephone[i]);
        if (feof(f))
            break;
        
        i++;
        *longueur += 1;
    }

    fclose(f);
}

void sauvegarde_fichier_adherant(int increment, int longueur, int id[], char nom[][TAILLE_MAX_NOM], char prenom[][TAILLE_MAX_PRENOM], int idcarte[], char adresse_email[][TAILLE_MAX_ADRESSE_EMAIL], char adresse[][TAILLE_MAX_ADRESSE], int code_postal[], int num_telephone[]){
    // Sauvegarde tous les lignes du fichier 'adherant.txt'.
    FILE * f=fopen(FICHIER_ADHERENT,"w");
    int i;

    if (f==NULL){

        // Test le chargement du fichier.
        err_fatal(ERR_CODE_FAIL_FICHIER, "Erreur de chargement fichier",  "consernant le fichier 'adhérant.txt'.");
    }
    
    fprintf(f, "%d\n", increment);

    for (i=0; i<longueur; i++){
        if (fprintf(f, "%d %s %s %d %s %s %d %d\n", id[i], nom[i], prenom[i], idcarte[i], adresse_email[i], adresse[i], code_postal[i], num_telephone[i]) < 0) {
            fclose(f);
            err_fatal(ERR_CODE_TABLE_SATUREE, "tableau trop petit/problème tableau", "sauvegarder donnees cartes");
        }
    }

    fclose(f);
}

char test_espace(char *chaine){
    // Verifie si il y a des espaces.
    
    while (*chaine) {
        if (*chaine == ' ')
            return 1;
        chaine++;
    }
    
    return 0;
}

void replace_espace_par_underscore(char chaine[]){
    // Remplace les espaces par des underscores.
    int i=0;
    while (chaine[i] != '\0'){
        if (chaine[i]==' '){
            chaine[i]='_';
            i++;
        }
        else i++;
    }
}

int not_exist(int id_adher){
    // Charge le fichier 'adherant.txt' dans des tableaux. 
    int increment, longueur;
    int id[TAILLE_MAX_FICHIER], idcarte[TAILLE_MAX_FICHIER], code_postal[TAILLE_MAX_FICHIER], num_telephone[TAILLE_MAX_FICHIER];
    char nom[TAILLE_MAX_FICHIER][TAILLE_MAX_NOM], prenom[TAILLE_MAX_FICHIER][TAILLE_MAX_PRENOM],  adresse_email[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE_EMAIL], adresse[TAILLE_MAX_FICHIER][TAILLE_MAX_ADRESSE];

    charger_fichier_adherant(&increment, TAILLE_MAX_FICHIER, &longueur, id, nom, prenom, idcarte, adresse_email, adresse, code_postal, num_telephone);

    int i=0;
    for (; i < longueur; i++)
        if (id[i] >= id_adher)
            return id[i] == id_adher ? 0 : -1;
    // id pas trouvé :
    return -1;
}

void supprimer_ligne(int * longueur, int index, int id[],char nom[][TAILLE_MAX_NOM],char prenom[][TAILLE_MAX_PRENOM], int idcarte[], char adresse_email[][TAILLE_MAX_ADRESSE_EMAIL], char adresse[][TAILLE_MAX_ADRESSE], int code_postal[], int num_telephone[]){

    // Localise la ligne 
    int localise = 0, i=0;
    while (i<*longueur){
        if (id[i] == index){
            localise = i;
        }
        else i++;
    }

    index = localise;

    // Supprime une ligne/adhérent dans les tableaux chargés.
    while(index<*longueur){
        id[index] = id[index+1];
        strcpy(nom[index], nom[index+1]);
        strcpy(prenom[index], prenom[index+1]);
        idcarte[index] = idcarte[index+1];
        strcpy(adresse_email[index], adresse_email[index+1]);
        strcpy(adresse[index],adresse[index+1]);
        code_postal[index] = code_postal[index+1];
        num_telephone[index] = num_telephone[index+1];

        index++;
    }

    *longueur -= 1;
}

int demande_id_adherent(void){

    // demande l'id de l'adhérent voulu.
    int id = -1;
    printf("Veuillez saisir un id adherent :\n");
    scanf("%d", &id);
    return id;
}