/**
 * \file commun.h
 * \brief Module concernant les function communes.
 * \author DELAGE Mathis MOULIN Mathis GROUSSEAU Mathieu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Les codes d'erreurs
#define ERR_CODE_FAIL_FICHIER 1
#define ERR_CODE_TABLE_SATUREE 1
#define ERR_CODE_ECRITURE_DANS_TABLEAU 1
//

/**
 * \brief Affiche un message d'erreur non fatale et continue l'execution du programme
 *
 * \param [in] msg le message d'erreur
 * \param [in] context Un contexte (optionnel), NULL si pas spécifié
 */
void err_normal(char *msg, const char *context);

/**
 * \brief Affiche un message d'erreur fatale et quitte le programme
 *
 * \param code Le code de sortie
 * \param [in] msg Le message d'erreur
 * \param [in] context Un contexte (optionnel), NULL si pas spécifié
 */
void err_fatal(int code, const char *msg, const char *context);

/**
 * \brief Lis et écrit un entier si spécifié
 *
 * \param [out] val La valeur a lire
 * \param [in] err_context Un contexte (pour les messages d'erreurs) (optionnel), NULL si pas spécifié
 * \return valeur non-zéro si une valeur a été lu, 0 si rien n'a été lu. EOF ==> 0
 *
 * Cette fonction écrit une valeur si et seulement si l'utilisateur écrit quelque chose.
 * Si rien n'est écrit, rien n'est écrit à l'adresse du pointeur.
 */
char maybe_scanf_int(int *val, const char *err_context);

/**
 * \brief Supprime la première occurence d'un lf dans le string
 * \param [in,out] str Le string
 * \return le string
 */
char *suppr_retour_a_la_ligne(char *str);

/**
 * \brief Pose que question et attends la réponse (Oui/Non) de l'utilisateur
 * \param [in] question la question a poser.
 * \return 0 si la réponse est non, !0 autrement si oui.
 */
char ask(char *question);

// void debug_string(char *str);